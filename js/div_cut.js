$(function(){
	
	//常规轮播图使用 无圆圈，无左右点击图
	var swiper = new Swiper('.banner_1', {
		speed:2000,
		loop: true,
		effect : 'coverflow',
      autoplay: {
        delay: 3000,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });
    //左右滑动实现
    var swiper1 = new Swiper('.menu', {
      slidesPerView: 5,
      spaceBetween: 50,
      // init: false,
      breakpoints: {
        1024: {
          slidesPerView: 5,
          spaceBetween: 40,
        },
        768: {
          slidesPerView: 5,
          spaceBetween: 25,
        },
        640: {
          slidesPerView: 5,
          spaceBetween: 15,
        },
        320: {
          slidesPerView: 5,
          spaceBetween: 10,
        }
      },
      scrollbar: {
        el: '.swiper-scrollbar',
        hide: true,
        draggable: true,
        hide: true,
//      dragSize: 30,//指示条长度
      },
      autoplay: false,//可选选项，自动滑动
    });
//  swiper1.scrollbar.$el.css('height','15px');设置元素高度
//mySwiper.scrollbar.$dragEl.css('background','#ff6600');设置颜色
//mySwiper.scrollbar.updateSize();
    var swiper2 = new Swiper('.message1', {
    	speed:3000,
    	direction: 'vertical',
    	effect : 'slide',
    	loop: true,
	  	autoplay:true,
    	autoplay: {
	    	delay: 3000,
		    stopOnLastSlide: false,
		    disableOnInteraction: true,
	    },
    });
});
