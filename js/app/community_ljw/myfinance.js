$(function(){
	//顶部事件加载
	$("body").on("click",".geren",qiehuan1);
	$("body").on("click",".jigou",qiehuan2);
	
	$("body").on("click",".tongyi",tongyi);
	$("body").on("click",".butongyi",butongyi);
	
//	//意向事件加载
});
function tongyi(){
	$(".tongyi").css("display","none");
	$(".butongyi").css("display","block");
}

function butongyi(){
	$(".tongyi").css("display","block");
	$(".butongyi").css("display","none");
}

function qiehuan1(){
	$(".geren").css("background-color","#faac14")
	$(".geren").css("color","#ffffff")
	
	$(".jigou").css("background-color","#ffffff")
	$(".jigou").css("color","#faac14")
	$(".title").text("我要资助(个人)");
	$(".top_ysy_c").css("width","60%");
}
function qiehuan2(){
	$(".jigou").css("background-color","#faac14")
	$(".jigou").css("color","#ffffff")
	
	$(".geren").css("background-color","#ffffff")
	$(".geren").css("color","#faac14")
	$(".title").text("我要资助(机构/企业)");
	$(".top_ysy_c").css("width","73%");
}

//时间选择-----------------------------------------
 	var selectDateDom = $('.selectDate');
    var showdata_ysy = $('.dateValue');
    // 初始化时间
    var now = new Date();
    var nowYear = now.getFullYear();
    var nowMonth = now.getMonth() + 1;
    var nowDate = now.getDate();
    
    // 数据初始化
    function formatYear (nowYear) {
        var arr = [];
        for (var i = nowYear - 5; i <= nowYear + 5; i++) {
            arr.push({
                id: i + '',
                value: i + '年'
            });
        }
        return arr;
    }
    function formatMonth () {
        var arr = [];
        for (var i = 1; i <= 12; i++) {
            arr.push({
                id: i + '',
                 value: i + '月'
            });
        }
        return arr;
    }
    function formatDate (count) {
        var arr = [];
        for (var i = 1; i <= count; i++) {
            arr.push({
                id: i + '',
                 value: i + '日'
            });
        }
        return arr;
    }
    var yearData = function(callback) {
        callback(formatYear(nowYear))
    }
    var monthData = function (year, callback) {
        callback(formatMonth());
    };
    var dateData = function (year, month, callback) {
        if (/^(1|3|5|7|8|10|12)$/.test(month)) {
            callback(formatDate(31));
        }
        else if (/^(4|6|9|11)$/.test(month)) {
            callback(formatDate(30));
        }
        else if (/^2$/.test(month)) {
            if (year % 4 === 0 && year % 100 !==0 || year % 400 === 0) {
                callback(formatDate(29));
            }
            else {
                callback(formatDate(28));
            }
        }
        else {
            throw new Error('month is illegal');
        }
    };
    selectDateDom.bind('click', function () {
	      var iosSelect = new IosSelect(3, 
			[yearData, monthData, dateData],
            {
                title: '时间选择',
                itemHeight: 35,
                itemShowCount: 9,
                callback: function (selectOneObj, selectTwoObj, selectThreeObj, selectFourObj, selectFiveObj) {
                    showdata_ysy.val(selectOneObj.value+''+selectTwoObj.value+selectThreeObj.value);
                }
        });
    });
    
//意向选择--------------------------------------
	var showBankDom1 = $('#showyixiang');
    var bankIdDom1 = $('#yixiangId');
    	showBankDom1.bind('click', function () {
//      var bankId1 = showBankDom1.dataset['id'];
//      var bankName1 = showBankDom1.dataset['value'];
		var bankId1 = showBankDom1.attr['id'];
        var bankName1 = showBankDom1.attr['value'];

        var bankSelect1 = new IosSelect(1, 
            [yixiangdata],
            {
                container: '.container',
                title: '资助意向选择',
                itemHeight: 50,
                itemShowCount: 3,
                oneLevelId: bankId1,
                showAnimate:true,
                callback: function (selectOneObj) {
//                  bankIdDom1.value = selectOneObj.id;
//                  showBankDom1.innerHTML = selectOneObj.value;
//                  showBankDom1.dataset['id'] = selectOneObj.id;
//                  showBankDom1.dataset['value'] = selectOneObj.value;
					bankIdDom1.val(selectOneObj.value);
                }
        });
    });
    
//性别选择————————————————————————————————————
    var showBankDom2 = $('#xingbiexuanze');
    var bankIdDom2 = $('#xingbieId');
    	showBankDom2.bind('click', function () {
        var bankId2 = showBankDom2.attr['id'];
        var bankName2 = showBankDom2.attr['value'];

        var bankSelect2 = new IosSelect(1, 
            [xingbiedata],
            {
                container: '.container',
                title: '资助意向选择',
                itemHeight: 50,
                itemShowCount: 3,
                oneLevelId: bankId2,
                showAnimate:true,
                callback: function (selectOneObj) {
					bankIdDom2.val(selectOneObj.value);
                }
        });
    });
////求助理由
//  var showBankDom3 = $('#qiuzhuliyou');
//  var bankIdDom3 = $('#qiuzhuliyouId');
//  	showBankDom3.bind('click', function () {
//      var bankId3 = showBankDom3.attr['id'];
//      var bankName3 = showBankDom3.attr['value'];
//
//      var bankSelect3 = new IosSelect(1, 
//          [qiuzhudata],
//          {
//              container: '.container',
//              title: '资助意向选择',
//              itemHeight: 50,
//              itemShowCount: 3,
//              oneLevelId: bankId3,
//              showAnimate:true,
//              callback: function (selectOneObj) {
//					bankIdDom3.val(selectOneObj.value);
//              }
//      });
//  });
    
//地址选择____________________________
    var selectContactDom = $('#select_contact');
    var showContactDom = $('#show_contact');
    selectContactDom.bind('click', function () {
        var sccode = showContactDom.attr('data-city-code');
        var scname = showContactDom.attr('data-city-name');

        var oneLevelId = showContactDom.attr('data-province-code');
        var twoLevelId = showContactDom.attr('data-city-code');
//      var threeLevelId = showContactDom.attr('data-district-code');
        var iosSelect = new IosSelect(2, 
            [iosProvinces, iosCitys, iosCountys],
            {
                title: '地址选择',
                itemHeight: 35,
                relation: [1, 1],
                oneLevelId: oneLevelId,
                twoLevelId: twoLevelId,
//              threeLevelId: threeLevelId,
                callback: function (selectOneObj, selectTwoObj, selectThreeObj) {
					showContactDom.val(selectOneObj.value + ' ' + selectTwoObj.value);
                }
        });
    });
    

//图片上传
var picArr = new Array(); // 存储图片
		$('input:file').localResizeIMG({
			width: 800, // 宽度
			quality: 1, // 压缩参数 1 不压缩 越小清晰度越低
			success: function(result) {
				var img = new Image();
				img.src = result.base64;
				var _str = "<span class='pic_look' style='background-image: url(" + img.src + ")'><div class='chacha' id='delete_pic'>×</div></span>"
				$('#chose_pic_btn').before(_str);
				var _i = picArr.length
				picArr[_i] = result.base64;
				picArr[_i] = _i;
				console.log(picArr)

			}
		});
		// 删除
		$(document).on('click', '#delete_pic', function(event) {
			var aa = $(this).parents(".pic_look").index();
			picArr.splice(aa, 1);
			$(this).parents(".pic_look").remove();
			console.log(picArr);
});