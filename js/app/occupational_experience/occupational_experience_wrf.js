$(function() {
	$("#menu_1").on("click", menu2);
	$("#menu_2").on("click", menu2);
	$("#menu_3").on("click", menu2);
	$("#menu_4").on("click", menu2);
//	$("#menu_5").on("click", menu);
	$("body").on("click", ".sousuoimg", arise);
	$(".header_banner,.tuijian,.footer,.job_list").on("click", vanish);
	$(".searchI,.header_banner,.tuijian,.footer,.job_list").on("click", list_vanish);

	$(".menu-list-item a").on("click", change_xian);

//	$("body").on("click", "#menu_1", link1);
	tuijian();

	//菜单侧滑======================
	var menu = function(wrap, menuList, menuItems) {
		var deviceWidth = $(window).width();
		var positionX = 0;
		var menuListPositionX1 = wrap.offset().left;
		var menuListPositionX2 = menuListPositionX1 + wrap.width();

		$(menuList).attr("style", "transition-duration: 0ms;transform: translateX(0px);");

		menuList.addEventListener('touchstart', function(event) {
			if(event.targetTouches.length == 1) {
				var touch = event.targetTouches[0];
				positionX = touch.pageX;
				//确定本次拖动transform的初始值
				var transformStr = menuList.style.transform;
				transformStr = transformStr.substring(11);
				var index = transformStr.lastIndexOf("p");
				transformStr = transformStr.substring(0, index);
				transformX = parseInt(transformStr);
				//确定本次拖动的div宽度值
				var widthStr = menuList.style.width;
				thisWidth = parseInt(widthStr.substring(0, widthStr.lastIndexOf("p")));
			}
		}, false);
		menuList.addEventListener('touchmove', function(event) {
			//阻止其他事件
//			event.preventDefault();
			//获取当前坐标
			if(event.targetTouches.length == 1) {
				var touch = event.targetTouches[0];
				menuList.style.transform = 'translateX(' + (transformX + touch.pageX - positionX) + 'px)';
				$(menuList).css("width", thisWidth + positionX - touch.pageX);
			}
		}, false);
		menuList.addEventListener('touchend', function(event) {
			var menuItem1 = menuItems[0];
			var menuItem1Left = $(menuItem1).offset().left;
			var menuItem2 = menuItems[menuItems.length - 1];
			var menuItemPositionX = $(menuItem2).offset().left + $(menuItem2).width();
			var firstToLast = menuItemPositionX - menuItem1Left;
			if(menuItem1Left > menuListPositionX1 || firstToLast < deviceWidth) {
				menuList.style.transform = 'translateX(' + (menuListPositionX1) + 'px)';
			}
			if(menuItemPositionX < menuListPositionX2 && menuItem1Left < 0 && firstToLast > deviceWidth) {
				var myWidth = $(menuList).width() - deviceWidth;
				menuList.style.transform = 'translateX(' + (0 - myWidth) + 'px)';
			}
		}, false);
	}

	var event = function() {
		$('#menu-wrap .js-menu-list a').click(function() {
			console.log($(this).attr('data-index'));
			var activeMenu = $('.js-menu-wrap' + $(this).attr('data-index'));
			activeMenu.siblings().hide();
			activeMenu.show();
		});
	}
	var init = function() {
		var menuWrap = $('#menu-wrap');
		var menuList = $("#menu-wrap .js-menu-list")[0];
		var menuListItems = $("#menu-wrap .menu-list-item");
		menu(menuWrap, menuList, menuListItems);

		var menuWrapLayer2 = $('.js-menu-wrap-layer2');
		var helper = function(tempWrap, tempMenuList, tempMenuListItems) {
			var tempFun = function() {
				menu(tempWrap, tempMenuList, tempMenuListItems);
			}();
		}
		for(var i = 0; i < menuWrapLayer2.length; i++) {
			var tempWrap = $(menuWrapLayer2[i]);
			var tempMenuList = $(menuWrapLayer2[i]).find('.js-menu-list')[0];
			var tempMenuListItems = $(menuWrapLayer2[i]).find('.menu-list-item');
			helper(tempWrap, tempMenuList, tempMenuListItems);
		}
		event();
	}();

});

var state = 1;

function menu2() {
	var obj = this;
	state++;
	/*把四格图标换成灰色*/
	$("#menu_1 img").attr("src", "../../../img/app/occupational_experience/daohang/shouye.png");
	$("#menu_2 img").attr("src", "../../../img/app/occupational_experience/daohang/shoucang.png");
	$("#menu_3 img").attr("src", "../../../img/app/occupational_experience/daohang/fabujianzhi.png");
	$("#menu_4 img").attr("src", "../../../img/app/occupational_experience/daohang/wode.png");
	$("#menu_1 span").css("color", "rgb(153,153,153)");
	$("#menu_2 span").css("color", "rgb(153,153,153)");
	$("#menu_3 span").css("color", "rgb(153,153,153)");
	$("#menu_4 span").css("color", "rgb(153,153,153)");
	var index = "1";
	if(state != 1) {
		index = $(obj).attr("id").split("_")[1];
	}
	var url = "";
	switch(index) {
		case "1":
			url = "../../../img/app/occupational_experience/daohang/shouyese.png";
			break;
		case "2":
			url = "../../../img/app/occupational_experience/daohang/shoucangse.png";
			break;
		case "3":
			url = "../../../img/app/occupational_experience/daohang/fabujianzhise.png";
			break;
		case "4":
			url = "../../../img/app/occupational_experience/daohang/wodese.png";
			break;
	}
	if(state != 1) {
		$(obj).children().eq(0).children().eq(0).attr("src", url);
		$(obj).children().eq(1).css("color", "rgb(250,172,20)");
	} else {
		$("#menu_1").children().eq(0).attr("src", url);
	}
}

function showCity() {
	$("#citys").show();
}

function hideCity(m) {
	$('#citys').hide();
	$(".location").text($(m).text());

}

function arise() {
	$(".sousuoimg").hide();
	$("#sou_input").show();
	$(".sousuo2").show();
}

function vanish() {
	$(".sousuoimg").show();
	$("#sou_input").hide();
	$(".sousuo2").hide();
}

function list_vanish() {
	$('#citys').hide();
}

function link1() {
	window.location.href = "../index.html";
}

function change_xian() {
	var obj1 = this;
	var obj2 = $(obj1).parent();
	/*把底线全部变成消失*/
	$(".menu-list-item").css("border-bottom-width", "0px");
	$(obj2).css("border-bottom-width", "3px");

}

function tuijian() {
	$(".menu-list-item").eq(0).css("border-bottom-width", "3px");
}

/*返回顶部js*/
$(document).scroll(function(e) {
	if($(document).scrollTop()>400){
        $(".back").show();
    }else{
    	$(".back").hide();
    }
});